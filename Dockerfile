FROM registry.gitlab.com/ninekflames/java-18-temurin-dind

ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

ARG MAVEN_VERSION="3.8.5"

RUN apk update &&\
    apk upgrade &&\
    apk add --no-cache bash

RUN apk add --no-cache --virtual=build-dependencies openjdk8 && \
    apk add "maven=$MAVEN_VERSION-r0" && \
    apk del build-dependencies \

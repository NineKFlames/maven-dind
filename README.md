# Maven Dind

This repository contains a Dockerfile for a Docker-in-Docker image with Maven installed to enable Testcontainers use during tests.

CI is also in place for publishing the images to the project-local registry.

Image tag: `registry.gitlab.com/ninekflames/maven-dind:latest` 

Example usage: https://gitlab.com/NineKFlames/minimal-sba-with-cucumber/-/blob/master/.gitlab-ci.yml

### Credits:

* https://github.com/tloist/docker-alpine-sbt  
* https://github.com/adoptium/containers  
* https://gitlab.com/NineKFlames/sbt-dind
